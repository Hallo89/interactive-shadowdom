# InteractiveShadowDOM
InteractiveShadowDOM (ISD) is a self-contained web component that creates an editable
[shadow DOM](https://developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_shadow_DOM).
It doesn't require any dependencies.

By default, it takes JavaScript, HTML and CSS code from separate input panels and features
an additional output panel that shows a highlighted HTML tree of the resulting DOM.
However, InteractiveShadowDOM is deliberately held very generic so that you can easily
create your own input and output panels which will communicate with each other by default.

This project comes with a very handy Markdown plugin that replaces specially marked code blocks
with the HTML code of the InteractiveShadowDOM. Read more [below](#markdown-plugin).


## Installation
InteractiveShadowDOM isn't on any package manager. It has a rolling release model,
which means you can always fetch the files from the `main` branch of this repository.
Thus, you should pin your versions to a specific commit.

### Download
When you don't need any special files
(either the [CodeMirror compilation code](#compiling-codemirror) or the [markdown plugin](#markdown-plugin)),
you can download the `./web/out/` folder. It contains everything you need for the front end,
including type definitions.

### Git submodules
If your project is a Git repository already, you can easily use
[Git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules), which is a
powerful tool capable of replacing a package manager for simple projects. That way, you
can pin this repo to a specific commit and only update it when you're ready.

Setup:
```sh
cd ./path/to/your/repo
git submodule add https://gitlab.com/Hallo89/utility-wheel.git
```

Update (this will fetch the latest commit of `main`):
```sh
git submodule update --remote
```

All required files for the front-end are in `./web/out/`, including type definitions.


## Prerequisites
InteractiveShadowDOM does not build its DOM for you. It is your job to include the
provided HTML template (`./web/out/html/template.html`) into your project.
You can also copy-paste the snippet into your document if you are fine with potentially
updating it in the future if something changes.

When working with Markdown, you can use the provided [markdown plugin](#markdown-plugin).


## Setup
InteractiveShadowDOM can be configured with a simple configuration and an advanced one. Those two approaches may not be mixed together.

The simple config will result in a simple interactive shadow DOM with the built-in
Inputs/Outputs (IOs) as demonstrated above,
while the advanced config lets you specify your own mixture of IOs.

### Simple setup
[TODO REFER TO DOCS]

Configuration with the default options:
```js
// Import InteractiveShadowDOM

const wrapper = document.querySelector('.interactive-shadow-dom');
new InteractiveShadowDOM(wrapper, {
  // Takes the initial source code for the inputs
  source: {
    js: '',
    css: '',
    html: '',
  },
  // Array of persistent CSS code strings that always stay in the shadow DOM
  staticStyleSheets: [],
  // Initially active input panel. Available keywords: 'js', 'html', 'css'
  activeInput: 'js',
  // Initially active output panel. Available keywords: 'dom', 'html'
  activeOutput: 'dom'
});
```

This will create the default interactive shadow DOM as demonstrated in the introduction.

The resulting instance won't be of much interest except for developing/debugging.


### Advanced setup
When escaping the simple configuration, you can specify the exact Inputs and Outputs (IOs)
that will be part of the InteractiveShadowDOM. Let's see the default setup from above,
but configured explicitly within the advanced config:

```js
// Import InteractiveShadowDOM, io/CodeInput, io/DOMOutput, io/HTMLOutput

const wrapper = document.querySelector('.interactive-shadow-dom');

const domOutput = new DOMOutput(/* staticStyleSheets? */);
const jsInput = new CodeInput('js', '');

new InteractiveShadowDOM(wrapper, {
  // Arbitrarily many Inputs in order of DOM precedence. They have to extend `InputBase`.
  inputs: [
    jsInput,
    new CodeInput('html', ''),
    new CodeInput('css', ''),
  ],
  // Arbitrarily many Outputs in order of DOM precedence. They have to extend `OutputBase`.
  outputs: [
    domOutput,
    new HTMLOutput(domOutput.contentNode)
  ],
  // One Output out of `outputs` that will be active intially (Default: First Output)
  activeOutput: domOutput,
  // One Input out of `inputs` that will be active intially (Default: First Input)
  activeInput: jsInput
});
```

You can see that by constructing each IO by hand, we have a lot more control
over how the ISD will function. Extending this example, we could then add
additional `CodeInput`s or write our own Inputs entirely.


## Compiling CodeMirror
[TODO]

## Markdown plugin
[TODO]


## Documentation
The project is well-documented. See [TODO] for a full project reference.

If you feel like the documentation lacks important aspects or leaves things in the
unclear, please open an issue or pull request!


## Styling
Every HTML class is prefixed with `isd-`.

### CSS Variables
There are some CSS variables available that should cover the most useful use cases:

```css
:root {
  --isdom-gap: .45em;
  --isdom-focus-color: #1960C9;
  --isdom-panel-font-size: .915rem;
  --isdom-panel-border-radius: 9px;
  --isdom-panel-min-height: 17.25em;
  --isdom-panel-background: hsl(0, 0%, 4%);
  --isdom-tab-background: hsl(0, 0%, 20%);
  --isdom-tab-background-hover: hsl(0, 0%, 18%);
}
```
