import path from 'path';
import { build } from 'esbuild';
import { fileURLToPath } from 'url';


const cwd = path.dirname(fileURLToPath(import.meta.url));

await build({
  entryPoints: [ path.join(cwd, 'codemirror/target.js') ],
  bundle: true,
  minify: true,
  format: 'esm',
  target: 'esnext',
  outfile: './web/out/script/codemirror.bundle.js',
});
