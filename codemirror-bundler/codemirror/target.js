import { css } from '@codemirror/lang-css';
import { html } from '@codemirror/lang-html';
import { javascript } from '@codemirror/lang-javascript';
import { EditorView, basicSetup } from 'codemirror';
import { moonfly } from 'codemirror-theme-moonfly';

export default {
  basicSetup,
  EditorView,
  lang: {
    js: javascript(),
    ts: javascript({typescript: true}),
    html: html(),
    css: css()
  },
  theme: {
    moonfly
  }
};
