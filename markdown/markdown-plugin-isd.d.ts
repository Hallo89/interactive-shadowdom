export interface AttributeDescriptor {
    pattern: RegExp;
    /**
     * If set, the whole token is skipped if its info (everything behind the ```)
     * does not satisfy the defined pattern
     */
    mandatory?: boolean;
}
export type AttributeList = Record<string, AttributeDescriptor>;
export type Attributes = Record<string, Map<RegExpMatchArray, any>>;
export interface UserConfig {
    /**
     * Is called when the token immediately following the current token
     * (which is an interactive code block) is an interactive code block.
     * - Return false to not further modify the token.
     * - Return true to replace the current token with the empty string, deleting the
     *   interactive code block from the document and storing its language and contents
     *   before stepping into the next token.
     *
     * You can use this to accumulate concurrent interactive code blocks
     * with a constraint of your choice, only invoking {@link replacer} on the last one.
     *
     * See the source of {@link defaultReplacerBeforeNext} for inspiration.
     *
     * @defaultValue
     * Returns true when the code block is marked as `append`, false otherwise.
     *
     * @param token The current token.
     * @param nextToken The next token (the symbol immediately following {@link token}).
     * @param attributes The (so far) resolved attributes following the attribute list.
     * @return { boolean }
     */
    replacerBeforeNext: (token: any, nextToken: any, attributes: Attributes) => boolean;
    /**
     * Takes all accumulated interactive {@link Attributes}
     * (See {@link replacerBeforeNext}) along with their content and returns the
     * string the current token should be replaced with.
     * Return false to not replace anything but clear the detected langs object.
     *
     * @param attributes The resolved attributes following the attribute list.
     * @return { string | false }
     */
    replacer: (attributes: Attributes, token: any) => string | false;
}
/**
 * Accepts patterns that are matched against an interactive code block's
 * information (the content after the three backticks) and, if present,
 * stored in an attribute map of type {@link Attributes} that is passed
 * to {@link UserConfig.replacer} and {@link UserConfig.replacerBeforeNext}.
 */
export declare const attributeList: AttributeList;
export default function (md: any, config?: Partial<UserConfig>): void;
export declare function defaultReplacerBeforeNext(token: any, nextToken: any): boolean;
export declare function defaultReplacer(attribs: Attributes, token: any): string | false;
/** Test whether the given MarkdownIt token is an interactive code block. */
export declare function tokenIsInteractive(token: any): boolean;
/** Escape a code block's source. */
export declare function escapeSource(content: string): string;
