const HTML_CONTENT = `\
<div class="interactive-shadow-dom">
  <div class="isd-input-wrapper">
    <ul class="isd-tab-list">
      <li>
        <button class="isd-active" type="button">JS</button>
      </li>
      <li>
        <button type="button">HTML</button>
      </li>
      <li>
        <button type="button">CSS</button>
      </li>
    </ul>
    <div class="isd-panels">
      <div class="isd-active"></div>
      <div></div>
      <div></div>
    </div>
    <div class="isd-resize-bar"></div>
  </div>
  <div class="isd-output-wrapper">
    <ul class="isd-tab-list">
      <li>
        <button class="isd-active" type="button">Output</button>
      </li>
      <li>
        <button type="button">Output (HTML)</button>
      </li>
    </ul>
    <div class="isd-panels">
      <div class="isd-active"></div>
      <div></div>
    </div>
  </div>
</div>`;
const SNIPPET_DESTINATION_REGEX = /(interactive-shadow-dom+.)(>)/;
/**
 * Accepts patterns that are matched against an interactive code block's
 * information (the content after the three backticks) and, if present,
 * stored in an attribute map of type {@link Attributes} that is passed
 * to {@link UserConfig.replacer} and {@link UserConfig.replacerBeforeNext}.
 */
export const attributeList = {
    language: {
        pattern: /^(\w+)/,
        mandatory: true
    },
};
export default function (md, config = {}) {
    config.replacer ?? (config.replacer = defaultReplacer);
    config.replacerBeforeNext ?? (config.replacerBeforeNext = defaultReplacerBeforeNext);
    const defaultRender = md.renderer.rules.fence;
    let attributes = {};
    for (const name of Object.keys(attributeList)) {
        attributes[name] = new Map();
    }
    md.renderer.rules.fence = function (tokens, idx, options, env, self) {
        const token = tokens[idx];
        if (tokenIsInteractive(token)) {
            const nextToken = tokens[idx + 1];
            for (const [name, descriptor] of Object.entries(attributeList)) {
                if (!descriptor.pattern.test(token.info)) {
                    if (descriptor.mandatory) {
                        return defaultRender(tokens, idx, options, env, self);
                    }
                    else
                        continue;
                }
                const match = token.info.match(descriptor.pattern);
                attributes[name].set(match, token);
            }
            if (nextToken
                && tokenIsInteractive(nextToken)
                && (config.replacerBeforeNext(token, nextToken, attributes))) {
                return '';
            }
            if (attributes.language.size > 0) {
                const output = config.replacer(attributes, token);
                for (const attribEntry of Object.values(attributes)) {
                    attribEntry.clear();
                }
                if (output) {
                    return output;
                }
            }
        }
        return defaultRender(tokens, idx, options, env, self);
    };
}
// ---- Default replacers ----
export function defaultReplacerBeforeNext(token, nextToken) {
    if (/\sappend(?:\s|$)/.test(nextToken.info)) {
        return true;
    }
    return false;
}
export function defaultReplacer(attribs, token) {
    if (attribs.language.size === 0)
        return false;
    let dataSourceContent = '';
    for (const [[, langName], { content: langContent }] of attribs.language) {
        const safeLangContent = escapeSource(langContent);
        if (safeLangContent) {
            dataSourceContent += ` data-source-${langName}="${safeLangContent}"`;
        }
    }
    const result = HTML_CONTENT.replace(SNIPPET_DESTINATION_REGEX, (_, attributes, closingTag) => {
        // NOTE: `dataSourceContent` has a leading space if not empty, so this works!
        return attributes + dataSourceContent + closingTag;
    });
    return result;
}
// ---- Helper functions ----
/** Test whether the given MarkdownIt token is an interactive code block. */
export function tokenIsInteractive(token) {
    return token.tag === 'code' && /\sinteractive(?:\s|$)/.test(token.info);
}
/** Escape a code block's source. */
export function escapeSource(content) {
    return content
        .replaceAll('\\', '\\\\')
        .replaceAll('"', '&quot;');
}
