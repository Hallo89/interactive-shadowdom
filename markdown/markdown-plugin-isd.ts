const HTML_CONTENT = `\
<div class="interactive-shadow-dom">
  <div class="isd-input-wrapper">
    <ul class="isd-tab-list">
      <li>
        <button class="isd-active" type="button">JS</button>
      </li>
      <li>
        <button type="button">HTML</button>
      </li>
      <li>
        <button type="button">CSS</button>
      </li>
    </ul>
    <div class="isd-panels">
      <div class="isd-active"></div>
      <div></div>
      <div></div>
    </div>
    <div class="isd-resize-bar"></div>
  </div>
  <div class="isd-output-wrapper">
    <ul class="isd-tab-list">
      <li>
        <button class="isd-active" type="button">Output</button>
      </li>
      <li>
        <button type="button">Output (HTML)</button>
      </li>
    </ul>
    <div class="isd-panels">
      <div class="isd-active"></div>
      <div></div>
    </div>
  </div>
</div>`;


export interface AttributeDescriptor {
  pattern: RegExp
  /**
   * If set, the whole token is skipped if its info (everything behind the ```)
   * does not satisfy the defined pattern
   */
  mandatory?: boolean
}
export type AttributeList = Record<string, AttributeDescriptor>;
export type Attributes = Record<string, Map<RegExpMatchArray, any>>;

export interface UserConfig {
  /**
   * Is called when the token immediately following the current token
   * (which is an interactive code block) is an interactive code block.
   * - Return false to not further modify the token.
   * - Return true to replace the current token with the empty string, deleting the
   *   interactive code block from the document and storing its language and contents
   *   before stepping into the next token.
   *
   * You can use this to accumulate concurrent interactive code blocks
   * with a constraint of your choice, only invoking {@link replacer} on the last one.
   *
   * See the source of {@link defaultReplacerBeforeNext} for inspiration.
   *
   * @defaultValue
   * Returns true when the code block is marked as `append`, false otherwise.
   *
   * @param token The current token.
   * @param nextToken The next token (the symbol immediately following {@link token}).
   * @param attributes The (so far) resolved attributes following the attribute list.
   * @return { boolean }
   */
  replacerBeforeNext: (token, nextToken, attributes: Attributes) => boolean
  /**
   * Takes all accumulated interactive {@link Attributes}
   * (See {@link replacerBeforeNext}) along with their content and returns the
   * string the current token should be replaced with.
   * Return false to not replace anything but clear the detected langs object.
   *
   * @param attributes The resolved attributes following the attribute list.
   * @return { string | false }
   */
  replacer: (attributes: Attributes, token) => string | false
}


const SNIPPET_DESTINATION_REGEX = /(interactive-shadow-dom+.)(>)/;

/**
 * Accepts patterns that are matched against an interactive code block's
 * information (the content after the three backticks) and, if present,
 * stored in an attribute map of type {@link Attributes} that is passed
 * to {@link UserConfig.replacer} and {@link UserConfig.replacerBeforeNext}.
 */
export const attributeList: AttributeList = {
  language: {
    pattern: /^(\w+)/,
    mandatory: true
  },
};


export default function(md, config: Partial<UserConfig> = {}) {
  config.replacer ??= defaultReplacer;
  config.replacerBeforeNext ??= defaultReplacerBeforeNext;

  const defaultRender = md.renderer.rules.fence;

  let attributes: Attributes = {};
  for (const name of Object.keys(attributeList)) {
    attributes[name] = new Map();
  }


  md.renderer.rules.fence = function(tokens, idx, options, env, self) {
    const token = tokens[idx];

    if (tokenIsInteractive(token)) {
      const nextToken = tokens[idx + 1];

      for (const [ name, descriptor ] of Object.entries(attributeList)) {
        if (!descriptor.pattern.test(token.info as string)) {
          if (descriptor.mandatory) {
            return defaultRender(tokens, idx, options, env, self);
          } else continue;
        }
        const match = (token.info as string).match(descriptor.pattern);
        attributes[name].set(match!, token);
      }

      if (nextToken
          && tokenIsInteractive(nextToken)
          && (config.replacerBeforeNext!(token, nextToken, attributes))) {
        return '';
      }

      if (attributes.language.size > 0) {
        const output = config.replacer!(attributes, token);
        for (const attribEntry of Object.values(attributes)) {
          attribEntry.clear();
        }
        if (output) {
          return output;
        }
      }
    }

    return defaultRender(tokens, idx, options, env, self);
  }
}


// ---- Default replacers ----
export function defaultReplacerBeforeNext(token, nextToken) {
  if (/\sappend(?:\s|$)/.test(nextToken.info)) {
    return true;
  }
  return false;
}

export function defaultReplacer(attribs: Attributes, token) {
  if (attribs.language.size === 0) return false;

  let dataSourceContent = '';
  for (const [ [, langName ], { content: langContent } ] of attribs.language) {
    const safeLangContent = escapeSource(langContent);
    if (safeLangContent) {
      dataSourceContent += ` data-source-${langName}="${safeLangContent}"`
    }
  }

  const result = HTML_CONTENT.replace(SNIPPET_DESTINATION_REGEX, (_, attributes, closingTag) => {
    // NOTE: `dataSourceContent` has a leading space if not empty, so this works!
    return attributes + dataSourceContent + closingTag;
  });

  return result;
}


// ---- Helper functions ----
/** Test whether the given MarkdownIt token is an interactive code block. */
export function tokenIsInteractive(token): boolean {
  return token.tag === 'code' && /\sinteractive(?:\s|$)/.test(token.info);
}

/** Escape a code block's source. */
export function escapeSource(content: string) {
  return content
    .replaceAll('\\', '\\\\')
    .replaceAll('"', '&quot;');
}
