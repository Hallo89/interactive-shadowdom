export class IOBase {
    name;
    id;
    /**
     * The panel this IO is operating in.
     *
     * Will be set during the build step, immediately before {@link build} is called.
     */
    panel;
    /**
     * The tab associated with this IO.
     *
     * Will be set during the build step, immediately before {@link build} is called.
     */
    tab;
    isActive;
    /**
     * @param name The name that's used in the tab content.
     * @param id A unique identifier of the Input/Output.
     *           Is added with the `data-io` attribute onto the panel and tab elements.
     */
    constructor(name, id = name) {
        this.name = name;
        this.id = id;
    }
}
export class InputBase extends IOBase {
    #updateCallbacks = [];
    /**
     * Every callback registered this way will be invoked
     * when calling {@link invokeUpdate}.
     *
     * *NOTE*: This method is mainly used internally
     * and will probably not concern you – it can safely be ignored.
     *
     * @see {@link OutputEvents.update}
     * @internal
     */
    addUpdateEventListener(callback) {
        this.#updateCallbacks.push(callback);
    }
    /**
     * Invoke an `update` event. This will bubble to all registered Outputs,
     * invoking the {@link OutputEvents.update} event in each Output.
     */
    invokeUpdate() {
        for (const callback of this.#updateCallbacks) {
            callback(this);
        }
    }
}
export class OutputBase extends IOBase {
}
//# sourceMappingURL=IOBase.js.map