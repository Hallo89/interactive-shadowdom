import type { DOMLanguages } from './io/DOMOutput.d.ts';
import type { IOType, InputBase, OutputBase, OutputEvents, IOEvents } from './IOBase.d.ts';
type TupleOmit<T extends any[], E extends any> = T extends [] ? [] : T extends [infer Head, ...infer Tail] ? Head extends E ? TupleOmit<Tail, E> : [Head, ...TupleOmit<Tail, E>] : T;
type OmitEventParamIOBase<T extends any[]> = TupleOmit<TupleOmit<T, InputBase[]>, OutputBase[]>;
/**
 * Simple configuration.
 * May not be mixed up with {@link AdvancedConfig}.
 * @see {@link Config}
 */
export interface SimpleConfig {
    /**
     * The initial source strings for the default code inputs (js, css, html).
     */
    source: Record<DOMLanguages, string>;
    /**
     * Additional style sheets to include in the shadow DOM output.
     */
    staticStyleSheets: string[];
    /**
     * Which input will be active initially. One of 'js', 'html', 'css'.
     * @defaultValue 'js'
     */
    activeInput: DOMLanguages;
    /**
     * Which output will be active initially. Either 'dom' or 'html'.
     * @defaultValue 'dom'
     */
    activeOutput: ActiveOutputValues;
}
/**
 * Advanced configuration.
 * May not be mixed up with {@link SimpleConfig}.
 * @see {@link Config}
 */
export interface AdvancedConfig {
    /**
     * Custom inputs that must extend {@link InputBase}.
     * Will override the default inputs.
     */
    inputs: InputBase[];
    /**
     * Custom outputs that must extend {@link OutputBase}.
     * Will override the default outputs.
     */
    outputs: OutputBase[];
    /**
     * Which input will be active initially.
     * Needs to be an input present in {@link inputs}.
     * @defaultValue {@link inputs}[0]
     */
    activeInput: InputBase;
    /**
     * Which output will be active initially.
     * Needs to be an output present in {@link outputs}.
     * @defaultValue {@link outputs}[0]
     */
    activeOutput: OutputBase;
}
/**
 * Available configuration. Used in the constructor.
 *
 * You can choose from two configuration strategies,
 * a simple and an advanced configuration that each have their own properties.
 * Mixing simple and advanced properties will raise an Error.
 *
 * This separation enables a simple yet configurable setup when using the default
 * IOs, while still being fully configurable when doing more advanced stuff.
 *
 * @see {@link SimpleConfig}
 * @see {@link AdvancedConfig}
 */
export type Config = SimpleConfig | AdvancedConfig;
interface IOElements {
    section: HTMLElement;
    panelWrapper: HTMLElement;
    tabWrapper: HTMLElement;
}
interface ScopeBase {
    input: InputBase;
    output: OutputBase;
}
type ActiveOutputValues = 'dom' | 'html';
type Scope = keyof ScopeBase;
type EventMap<Events extends IOEvents<IOType> | OutputEvents> = {
    [Event in keyof Events]: OmitEventParamIOBase<Parameters<Events[Event]>>;
};
type OutputEventMap = EventMap<OutputEvents>;
type IOEventMap<IO extends IOType> = EventMap<IOEvents<IO>>;
export declare class InteractiveShadowDOMError extends Error {
    constructor(...args: any[]);
}
export declare class InteractiveShadowDOM {
    #private;
    wrapper: HTMLElement;
    resizeBar: HTMLElement;
    elementsIO: Record<Scope, IOElements>;
    activeIO: ScopeBase;
    inputs: AdvancedConfig['inputs'];
    outputs: AdvancedConfig['outputs'];
    constructor(wrapper: HTMLElement, config?: Partial<Config>);
    /**
     * Activate the given IO for the given scope ("input" or "output")
     * and deactive the currently active IO of that scope.
     *
     * The passed IO must exist in the given scope.
     * @param scope Which scope to operate on.
     * @param io The IO to activate. It must currently exist in the given scope.
     */
    activateIO<S extends Scope>(scope: S, io: ScopeBase[S]): void;
    /**
     * Invoke an IO's event.
     *
     * @remarks
     * Each event is called with a variable amount of (given) arguments
     * and all registered inputs and outputs.
     *
     * @param io The IO to invoke the event of.
     * @param name The event name.
     * @param eventArgs Arguments that are passed to the invoked event handler.
     */
    invokeSingleEvent<IO extends IOType, T extends keyof IOEventMap<IO>>(io: IO, name: T, ...eventArgs: IOEventMap<IO>[T]): void;
    /**
     * Invoke an event in all Outputs.
     *
     * @remarks
     * Each event is called with a variable amount of (given) arguments
     * and all registered inputs and outputs.
     *
     * @param name The event name.
     * @param eventArgs Arguments that are passed to the invoked event handler.
     */
    invokeOutputEvent<T extends keyof OutputEventMap>(name: T, ...eventArgs: OutputEventMap[T]): void;
    /** @internal */
    _resizeDown(e: PointerEvent): void;
    /** @internal */
    _resizeMove(e: PointerEvent): void;
    /** @internal */
    _resizeUp(e: PointerEvent): void;
    /**
     * Assert whether a passed config uses the simple or advanced strategy.
     *
     * @remarks
     * This will assert to a simple configuration when the passed object is empty!
     * @internal
     */
    static isAdvancedConfig(config: Partial<Config>): config is Partial<AdvancedConfig>;
    /**
     * Create an IO's tab and panel elements and append them to the DOM.
     * @param io The IO to create the elements for.
     * @param elements {@link IOElements} instance the tab and panel will get appended to.
     * @param callback The onclick callback. Its first argument gets bound to the given IO.
     * @return An object containing the created tab and panel elements.
     */
    static createTabAndPanel(io: IOType, elements: IOElements, callback: (io: IOType) => void): {
        panel: HTMLDivElement;
        tab: HTMLLIElement;
    };
    /**
     * Get all relevant DOM elements from the InteractiveShadowDOM's DOM wrapper.
     * @param baseElement The DOM wrapper resulting from the assumed HTML template.
     * @return An object containing the {@link baseElement} itself and the panel and tab wrapper.
     */
    static getDOMElements(baseElement: HTMLElement): {
        section: HTMLElement;
        panelWrapper: HTMLElement;
        tabWrapper: HTMLElement;
    };
}
export {};
