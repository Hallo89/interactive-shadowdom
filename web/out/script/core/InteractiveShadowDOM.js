import { CodeInput } from './io/CodeInput.js';
import { HTMLOutput } from './io/HTMLOutput.js';
import { DOMOutput } from './io/DOMOutput.js';
const COLLAPSE_THRESHOLD = 7.5;
export class InteractiveShadowDOMError extends Error {
    constructor(...args) {
        super(...args);
        this.name = this.constructor.name;
    }
}
export class InteractiveShadowDOM {
    wrapper;
    resizeBar;
    elementsIO = {
        input: null,
        output: null
    };
    activeIO = {
        input: null,
        output: null
    };
    inputs = [];
    outputs = [];
    constructor(wrapper, config = {}) {
        this._resizeDown = this._resizeDown.bind(this);
        this._resizeMove = this._resizeMove.bind(this);
        this._resizeUp = this._resizeUp.bind(this);
        this.activateIO = this.activateIO.bind(this);
        this.invokeOutputEvent = this.invokeOutputEvent.bind(this);
        this.wrapper = wrapper;
        this.elementsIO.input = InteractiveShadowDOM.getDOMElements(wrapper.querySelector('.isd-input-wrapper'));
        this.elementsIO.output = InteractiveShadowDOM.getDOMElements(wrapper.querySelector('.isd-output-wrapper'));
        this.resizeBar = this.elementsIO.input.section.querySelector('.isd-resize-bar');
        if (InteractiveShadowDOM.isAdvancedConfig(config)) {
            if (config.inputs)
                this.inputs = config.inputs;
            if (config.outputs)
                this.outputs = config.outputs;
            // NOTE: This can be undefined when inputs or outputs is of length 0
            this.activeIO.input = config.inputs.includes(config.activeInput) ? config.activeInput : this.inputs[0];
            this.activeIO.output = config.outputs.includes(config.activeOutput) ? config.activeOutput : this.outputs[0];
        }
        else {
            config.activeInput ??= 'js';
            config.activeOutput ??= 'dom';
            for (const lang of ['js', 'html', 'css']) {
                const source = config.source?.[lang] ?? '';
                const input = new CodeInput(lang, source);
                this.inputs.push(input);
                if (lang === config.activeInput) {
                    this.activeIO.input = input;
                }
            }
            const domOutput = new DOMOutput(config.staticStyleSheets);
            const htmlOutput = new HTMLOutput(domOutput.contentNode);
            this.outputs.push(domOutput, htmlOutput);
            this.activeIO.output = config.activeOutput === 'dom' ? domOutput : htmlOutput;
        }
        this.#initializeIO('output', this.outputs);
        this.#initializeIO('input', this.inputs, input => {
            input.addUpdateEventListener(this.invokeOutputEvent.bind(this, 'update'));
        });
        // Initialize Misc DOM events
        this.resizeBar.addEventListener('pointerdown', this._resizeDown);
        // Invoke output setup
        this.invokeOutputEvent('setup');
    }
    // ---- IO handling functions ----
    /**
     * Initialize the IOs for a given scope ("input" or "output").
     * @param scope Which scope to initialize.
     * @param ioList All IOs to initialize and assign to the scope.
     * @param callback Custom function called per and with every initialized IO.
     */
    #initializeIO(scope, ioList, callback) {
        const elements = this.elementsIO[scope];
        elements.tabWrapper.textContent = '';
        elements.panelWrapper.textContent = '';
        for (const io of ioList) {
            io.isActive = (io === this.activeIO[scope]);
            const boundClickHandler = this.activateIO.bind(this, scope);
            const { panel, tab } = InteractiveShadowDOM.createTabAndPanel(io, elements, boundClickHandler);
            io.panel = panel;
            io.tab = tab;
            callback?.(io);
            this.invokeSingleEvent(io, 'build', panel, tab);
        }
    }
    /**
     * Activate the given IO for the given scope ("input" or "output")
     * and deactive the currently active IO of that scope.
     *
     * The passed IO must exist in the given scope.
     * @param scope Which scope to operate on.
     * @param io The IO to activate. It must currently exist in the given scope.
     */
    activateIO(scope, io) {
        const activeIO = this.activeIO[scope];
        activeIO.tab.classList.remove('isd-active');
        activeIO.panel.classList.remove('isd-active');
        activeIO.isActive = false;
        io.tab.classList.add('isd-active');
        io.panel.classList.add('isd-active');
        io.isActive = true;
        this.activeIO[scope] = io;
        // @ts-ignore I don't get it
        this.invokeSingleEvent(io, 'activate', activeIO);
        // @ts-ignore
        this.invokeSingleEvent(activeIO, 'deactivate', io);
    }
    // ---- IO event handlers ----
    /**
     * Invoke an IO's event.
     *
     * @remarks
     * Each event is called with a variable amount of (given) arguments
     * and all registered inputs and outputs.
     *
     * @param io The IO to invoke the event of.
     * @param name The event name.
     * @param eventArgs Arguments that are passed to the invoked event handler.
     */
    invokeSingleEvent(io, name, ...eventArgs) {
        // @ts-ignore
        io[name]?.(...eventArgs, this.inputs, this.outputs);
    }
    /**
     * Invoke an event in all Outputs.
     *
     * @remarks
     * Each event is called with a variable amount of (given) arguments
     * and all registered inputs and outputs.
     *
     * @param name The event name.
     * @param eventArgs Arguments that are passed to the invoked event handler.
     */
    invokeOutputEvent(name, ...eventArgs) {
        for (const output of this.outputs) {
            // @ts-ignore
            output[name]?.(...eventArgs, this.inputs, this.outputs);
        }
    }
    // ---- DOM event handlers ----
    /** @internal */
    _resizeDown(e) {
        document.body.classList.add('isd-noselect');
        window.addEventListener('pointermove', this._resizeMove);
        window.addEventListener('pointerup', this._resizeUp);
    }
    /** @internal */
    _resizeMove(e) {
        const inputSection = this.elementsIO.input.section;
        const outputSection = this.elementsIO.output.section;
        const rect = this.wrapper.getBoundingClientRect();
        let delta = ((e.clientX - rect.left) / rect.width) * 100;
        inputSection.classList.remove('isd-collapsed');
        outputSection.classList.remove('isd-collapsed');
        if (delta <= COLLAPSE_THRESHOLD) {
            delta = 0;
            inputSection.classList.add('isd-collapsed');
        }
        else if (delta >= 100 - COLLAPSE_THRESHOLD) {
            delta = 100;
            outputSection.classList.add('isd-collapsed');
        }
        inputSection.style.width = delta + '%';
    }
    /** @internal */
    _resizeUp(e) {
        window.removeEventListener('pointermove', this._resizeMove);
        window.removeEventListener('pointerup', this._resizeUp);
        document.body.classList.remove('isd-noselect');
    }
    // ---- Static config handling ----
    /**
     * Assert whether a passed config uses the simple or advanced strategy.
     *
     * @remarks
     * This will assert to a simple configuration when the passed object is empty!
     * @internal
     */
    static isAdvancedConfig(config) {
        if ('inputs' in config || 'outputs' in config) {
            if ('source' in config || 'staticStyleSheets' in config) {
                throw new InteractiveShadowDOMError(`Simple and Advanced configuration may not be mixed!`);
            }
            return true;
        }
        return false;
    }
    // ---- Static DOM helpers ----
    /**
     * Create an IO's tab and panel elements and append them to the DOM.
     * @param io The IO to create the elements for.
     * @param elements {@link IOElements} instance the tab and panel will get appended to.
     * @param callback The onclick callback. Its first argument gets bound to the given IO.
     * @return An object containing the created tab and panel elements.
     */
    static createTabAndPanel(io, elements, callback) {
        const panel = document.createElement('div');
        const tab = document.createElement('li');
        const tabBtn = document.createElement('button');
        tabBtn.type = 'button';
        tabBtn.textContent = io.name;
        tabBtn.addEventListener('click', callback.bind(null, io));
        tab.appendChild(tabBtn);
        tab.dataset.io = io.id;
        panel.dataset.io = io.id;
        if (io.isActive) {
            tab.classList.add('isd-active');
            panel.classList.add('isd-active');
        }
        elements.tabWrapper.appendChild(tab);
        elements.panelWrapper.appendChild(panel);
        return { panel, tab };
    }
    /**
     * Get all relevant DOM elements from the InteractiveShadowDOM's DOM wrapper.
     * @param baseElement The DOM wrapper resulting from the assumed HTML template.
     * @return An object containing the {@link baseElement} itself and the panel and tab wrapper.
     */
    static getDOMElements(baseElement) {
        return {
            section: baseElement,
            panelWrapper: baseElement.querySelector('.isd-panels'),
            tabWrapper: baseElement.querySelector('.isd-tab-list')
        };
    }
}
//# sourceMappingURL=InteractiveShadowDOM.js.map