import CodeMirror from '~/codemirror.bundle.js';
import { InputBase } from '../IOBase.js';
type EditorViewInstance = InstanceType<typeof CodeMirror.EditorView>;
export declare class CodeInput extends InputBase {
    static IDLE_INTERVAL: number;
    editor: EditorViewInstance;
    timer: number;
    language: string;
    initialCode: string;
    constructor(language: string, initialCode?: string, name?: string);
    build(panel: HTMLElement): void;
    dispatchUpdateEvent(view: any): void;
    getSource(): any;
    static createEditor(target: HTMLElement, language: string, source: string, onUpdate: Function): EditorViewInstance;
}
export {};
