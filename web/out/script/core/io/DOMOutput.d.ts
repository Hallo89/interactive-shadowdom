import type { InputBase } from '../IOBase.d.ts';
import { OutputBase } from '../IOBase.js';
import { CodeInput } from './CodeInput.js';
export type DOMLanguages = 'html' | 'css' | 'js';
interface HTMLSourceMap {
    wrapper: HTMLDivElement;
    source: string;
}
/**
 * This Output creates an interactive shadow DOM with any {@link CodeInput}s
 * as its sources that correspond to either js, html or css.
 *
 * The shadow DOM is updated with the corresponding code input strings
 * when receiving a CodeInput's `update` event.
 * Alternatively, {@link updateState} can be used to manually invoke a update.
 */
export declare class DOMOutput extends OutputBase {
    #private;
    shadow: ShadowRoot;
    contentNode: HTMLDivElement;
    shadowContainer: HTMLDivElement;
    errorContainer: HTMLDivElement;
    cssSheets: Map<CodeInput, CSSStyleSheet>;
    htmlSources: Map<CodeInput, HTMLSourceMap>;
    jsFunctions: Map<CodeInput, () => void>;
    /** @internal */
    _staticCSSSources: string[];
    constructor(staticStyleSheets?: string[], name?: string);
    update(input: InputBase): void;
    build(panel: HTMLElement): void;
    setup(inputs: InputBase[]): void;
    /**
     * Bind the given {@link CodeInput} (either JS, HTML or CSS) to
     * the shadow DOM, processing its source.
     *
     * Except for the CSS, this does not yet update the Shadow DOM, only prepare
     * it for an update. You will want to call {@link updateState} after binding.
     */
    bindStateFromSingleInput(input: CodeInput): void;
    /** Reset the HTML content and call every bound JavaScript anew. */
    updateState(): void;
    /**
     * Set the content of the error modal and display it.
     * @param errorStr The error message.
     * @see {@link hideErrorContent}
     */
    setErrorContent(errorStr: string): void;
    /**
     * Hide the error modal again.
     * @see {@link setErrorContent}
     */
    hideErrorContent(): void;
    /**
     * Convert a JavaScript string to an executable function that is bound
     * to the current shadow DOM element as its `this`.
     *
     * Does not use `eval`.
     *
     * @see {@link shadowfyJSString}
     */
    convertJSStringToFunction(jsString: string): any;
    /**
     * Prepare a JavaScript string for use in a shadow DOM.
     *
     * Namely, this replaces all occurences of `document` with `this`
     * because a shadow DOM is not self contained and still exists
     * in the scope of the parent document
     * (Also see https://github.com/tc39/proposal-shadowrealm).
     *
     * @see {@link convertJSStringToFunction}
     */
    static shadowfyJSString(jsString: string): string;
    /**
     * Create a {@link CSSStyleSheet} with the given CSS source
     * for adoption by a shadom DOM.
     */
    static getStyleSheet(cssString: string): CSSStyleSheet;
    /**
     * Get the line and column position from an error. Highly unstable.
     *
     * @remarks
     * This reads the stack trace string given with an error.
     * It's really esotheric and heavily relies on vendor-specific implementation,
     * which is not a recipe for disaster at all. But hey, it works (for now)!
     *
     * Only works for Firefox and Chrome.
     *
     * @return [ line, column ].
     */
    static getAnonymousFnErrorPosition(error: Error): [number, number] | false;
}
export {};
