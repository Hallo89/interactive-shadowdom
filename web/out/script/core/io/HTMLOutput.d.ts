import { OutputBase } from '../IOBase.js';
/**
 * This Output shows a HTML tree from a bound DOM element.
 * It is continuously highlighted via highlight.js in a worker thread.
 *
 * A DOM element that's in the constructor is observed for changes.
 * Whenever a DOM update occurs, the inner HTML string is indented and then
 * highlighted in a parallel thread and then written to the output panel.
 */
export declare class HTMLOutput extends OutputBase {
    #private;
    static IDLE_TIMEOUT_INACTIVE: number;
    static IDLE_TIMEOUT_ACTIVE: number;
    codeTarget: HTMLElement;
    observableNode: Element;
    highlightWorker: Worker;
    observer: MutationObserver;
    timer: number;
    constructor(observableNode: Element, name?: string);
    /**
     * Pass the source of the bound element to the highlight worker after
     * {@link IDLE_TIMEOUT_ACTIVE} or {@link IDLE_TIMEOUT_INACTIVE} ms,
     * which then will update the output panel.
     *
     * Subsequent calls reset the interval timer, ensuring that the
     * HTML output gets updated iff the DOM has stopped being modified.
     *
     * @privateRemarks
     * IDEA: Make panel activatable IN ADDITION to DOM output panel.
     */
    updateHTMLOutput(): void;
    build(panel: HTMLElement): void;
    highlightWorkerMessage(e: MessageEvent): void;
    /**
     * Modify a given element such that its source is correctly indented.
     *
     * @remarks
     * Inspired by https://stackoverflow.com/a/26361620
     */
    static indentElementInPlace(element: Element): void;
}
