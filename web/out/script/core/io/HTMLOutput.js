import { OutputBase } from '../IOBase.js';
/**
 * This Output shows a HTML tree from a bound DOM element.
 * It is continuously highlighted via highlight.js in a worker thread.
 *
 * A DOM element that's in the constructor is observed for changes.
 * Whenever a DOM update occurs, the inner HTML string is indented and then
 * highlighted in a parallel thread and then written to the output panel.
 */
export class HTMLOutput extends OutputBase {
    /**
     * Private marker for TextNodes used in {@link indentElementInPlace}
     * to mark created text nodes as relevant for indentation.
     *
     * Possible values are 0 (node before an element) and 1 (node after an element).
     */
    static #INDENT_NODE_TYPE = Symbol("indentation-node");
    static IDLE_TIMEOUT_INACTIVE = 250;
    static IDLE_TIMEOUT_ACTIVE = 75;
    codeTarget;
    observableNode;
    highlightWorker;
    observer;
    timer;
    constructor(observableNode, name = 'Output (HTML)') {
        super(name, 'isd-html-output');
        this.highlightWorkerMessage = this.highlightWorkerMessage.bind(this);
        this.updateHTMLOutput = this.updateHTMLOutput.bind(this);
        this.observableNode = observableNode;
        this.highlightWorker = new Worker(new URL('./HTMLOutput.highlightHTML.worker.js', import.meta.url));
        this.highlightWorker.addEventListener('message', this.highlightWorkerMessage);
        this.observer = new MutationObserver(this.updateHTMLOutput.bind(this, false));
        this.observer.observe(this.observableNode, {
            subtree: true,
            attributes: true
        });
    }
    /**
     * Pass the source of the bound element to the highlight worker after
     * {@link IDLE_TIMEOUT_ACTIVE} or {@link IDLE_TIMEOUT_INACTIVE} ms,
     * which then will update the output panel.
     *
     * Subsequent calls reset the interval timer, ensuring that the
     * HTML output gets updated iff the DOM has stopped being modified.
     *
     * @privateRemarks
     * IDEA: Make panel activatable IN ADDITION to DOM output panel.
     */
    updateHTMLOutput() {
        const timeout = this.isActive
            ? HTMLOutput.IDLE_TIMEOUT_ACTIVE
            : HTMLOutput.IDLE_TIMEOUT_INACTIVE;
        if (this.timer != null) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            HTMLOutput.indentElementInPlace(this.observableNode);
            const source = this.observableNode.innerHTML.trim();
            this.highlightWorker.postMessage(source);
            this.timer = null;
        }, timeout);
    }
    // ---- Events ----
    build(panel) {
        this.codeTarget = panel
            .appendChild(document.createElement('pre'))
            .appendChild(document.createElement('code'));
        this.codeTarget.classList.add('language-html');
    }
    // ---- Mutation Observer ----
    highlightWorkerMessage(e) {
        this.codeTarget.innerHTML = e.data;
    }
    // ---- Static helpers ----
    /**
     * Modify a given element such that its source is correctly indented.
     *
     * @remarks
     * Inspired by https://stackoverflow.com/a/26361620
     */
    static indentElementInPlace(element) {
        for (const child of element.children) {
            this.#indentElement(child);
        }
    }
    static #indentElement(element, indentStep = 2, i = 1) {
        for (const child of Array.from(element.childNodes)) {
            if (child.nodeType === 3) {
                // @ts-ignore
                const type = child[this.#INDENT_NODE_TYPE];
                if (type != null) {
                    child.textContent = getIndent(i - type);
                }
            }
            else if (child.nodeType === 1) {
                // @ts-ignore
                if (child.previousSibling?.[this.#INDENT_NODE_TYPE] !== 0) {
                    child.before(createIndentNode(i, 0));
                }
                this.#indentElement(child, indentStep, i + 1);
                // @ts-ignore
                if (child.nextSibling?.[this.#INDENT_NODE_TYPE] !== 1) {
                    child.after(createIndentNode(i, 1));
                }
            }
        }
        function createIndentNode(level, type) {
            const node = document.createTextNode(getIndent(level - type));
            // @ts-ignore
            node[HTMLOutput.#INDENT_NODE_TYPE] = type;
            return node;
        }
        function getIndent(level) {
            return '\n' + ' '.repeat(indentStep * level);
        }
    }
}
//# sourceMappingURL=HTMLOutput.js.map