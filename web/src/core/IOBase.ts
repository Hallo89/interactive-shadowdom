export type IOType = InputBase | OutputBase;

type UpdateCallback = (input?: InputBase) => void;

// ---- Events ----
export interface IOEvents<IO extends IOType> {
  /**
   * Is called exactly once after `InteractiveShadowDOM` has finished
   * setting up all DOM and related operations.
   * @param panel The panel the IO is operating in.
   * @param tab The tab associated with the panel.
   * @param inputs All registered inputs.
   * @param outputs All registered outputs.
   */
  build(panel: HTMLElement, tab: HTMLElement, inputs: InputBase[], outputs: OutputBase[]): void
  /**
   * Is called after the IO has been actived by the user.
   * See {@link IOBase.isActive} for a way to asynchronously tell
   * whether the IO is currently active.
   * @param prevActive The IO that had been active before.
   * @param inputs All registered inputs.
   * @param outputs All registered outputs.
   */
  activate(prevActive: IO, inputs: InputBase[], outputs: OutputBase[]): void
  /**
   * Is called after the IO has been deactivated by the user
   * (by activating another IO).
   * See {@link IOBase.isActive} for a way to asynchronously tell
   * whether the IO is currently active.
   * @param nextActive Another IO that has been activated.
   * @param inputs All registered inputs.
   * @param outputs All registered outputs.
   */
  deactivate(nextActive: IO, inputs: InputBase[], outputs: OutputBase[]): void
}

export interface OutputEvents extends IOEvents<OutputBase> {
  /**
   * Is called every time an input calls its update event.
   * When extending inputs and/or outputs, you can use the arguments to
   * look for your desired input class using `instanceof`.
   * @param input The input that called the event.
   * @param inputs All registered inputs.
   * @param outputs All registered outputs.
   */
  update(input: InputBase, inputs: InputBase[], outputs: OutputBase[]): void
  /**
   * Is called exactly once after `InteractiveShadowDOM` has finished its
   * initial setup (calling `setup` the last operation in its constructor).
   * @param inputs All registered inputs.
   * @param outputs All registered outputs.
   */
  setup(inputs: InputBase[], outputs: OutputBase[]): void
}


// ---- Base ----
/**
 * Common class/interface every IO needs to extend.
 * This also allows for asserting objects to be an IO via `instanceof`.
 */
export interface IOBase<IO extends IOType> extends IOEvents<IO> {}
export class IOBase<IO extends IOType> {
  name: string;
  id: string;

  /**
   * The panel this IO is operating in.
   *
   * Will be set during the build step, immediately before {@link build} is called.
   */
  panel: HTMLElement;
  /**
   * The tab associated with this IO.
   *
   * Will be set during the build step, immediately before {@link build} is called.
   */
  tab: HTMLElement;

  isActive: boolean;

  /**
   * @param name The name that's used in the tab content.
   * @param id A unique identifier of the Input/Output.
   *           Is added with the `data-io` attribute onto the panel and tab elements.
   */
  constructor(name: string, id = name) {
    this.name = name;
    this.id = id;
  }
}

// ---- Input ----
/**
 * Common class/interface every Input needs to extend.
 * This also allows for asserting objects to be an Input via `instanceof`.
 */
export interface InputBase extends IOBase<InputBase> {}
export class InputBase extends IOBase<InputBase> {
  #updateCallbacks: UpdateCallback[] = [];

  /**
   * Every callback registered this way will be invoked
   * when calling {@link invokeUpdate}.
   *
   * *NOTE*: This method is mainly used internally
   * and will probably not concern you – it can safely be ignored.
   *
   * @see {@link OutputEvents.update}
   * @internal
   */
  addUpdateEventListener(callback: UpdateCallback) {
    this.#updateCallbacks.push(callback);
  }
  /**
   * Invoke an `update` event. This will bubble to all registered Outputs,
   * invoking the {@link OutputEvents.update} event in each Output.
   */
  invokeUpdate() {
    for (const callback of this.#updateCallbacks) {
      callback(this);
    }
  }
}

// ---- Output ----
/**
 * Common class/interface every Output needs to extend.
 * This also allows for asserting objects to be an Output via `instanceof`.
 */
export interface OutputBase extends OutputEvents, IOBase<OutputBase> {}
export class OutputBase extends IOBase<OutputBase> {}
