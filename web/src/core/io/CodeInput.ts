import CodeMirror from '~/codemirror.bundle.js';
import { InputBase } from '../IOBase.js';

type EditorViewInstance = InstanceType<typeof CodeMirror.EditorView>;

export class CodeInput extends InputBase {
  static IDLE_INTERVAL = 500;

  editor: EditorViewInstance;
  timer: number;
  language: string;

  initialCode: string;

  constructor(language: string, initialCode?: string, name?: string) {
    super(name ?? language.toUpperCase(), 'isd-code-input');

    this.dispatchUpdateEvent = this.dispatchUpdateEvent.bind(this);

    this.language = language;
    this.initialCode = initialCode ?? '';
  }

  // ---- Setup ----
  override build(panel: HTMLElement) {
    this.editor = CodeInput.createEditor(
      panel, this.language, this.initialCode, this.dispatchUpdateEvent);

    delete this.initialCode;
  }

  // ---- CodeMirror ----
  dispatchUpdateEvent(view: any) {
    // TODO This is wrong (See CodeMirror.ViewUpdate)
    if (view.changedRanges.length > 0) {
      if (this.timer != null) {
        clearTimeout(this.timer);
      }

      this.timer = setTimeout(() => {
        this.invokeUpdate();

        this.timer = null;
      }, CodeInput.IDLE_INTERVAL);
    }
  }

  getSource() {
    return this.editor.state.doc.toString();
  }


  // ---- Static initialization functions ----
  static createEditor(target: HTMLElement, language: string, source: string, onUpdate: Function): EditorViewInstance {
    const extensions = [
      CodeMirror.basicSetup,
      // CodeMirror.EditorView.lineWrapping,
      CodeMirror.theme.moonfly,
      CodeMirror.EditorView.updateListener.of(onUpdate),
    ];
    if (language in CodeMirror.lang) {
      // @ts-ignore
      extensions.push(CodeMirror.lang[language]);
    }

    return new CodeMirror.EditorView({
      extensions,
      doc: source,
      parent: target,
    });
  }
}
