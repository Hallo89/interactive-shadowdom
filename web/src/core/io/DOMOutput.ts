import type { InputBase } from '../IOBase.d.ts';
import { OutputBase } from '../IOBase.js';
import { CodeInput } from './CodeInput.js';

export type DOMLanguages = 'html' | 'css' | 'js';

interface HTMLSourceMap {
  wrapper: HTMLDivElement;
  source: string;
}

/**
 * This Output creates an interactive shadow DOM with any {@link CodeInput}s
 * as its sources that correspond to either js, html or css.
 *
 * The shadow DOM is updated with the corresponding code input strings
 * when receiving a CodeInput's `update` event.
 * Alternatively, {@link updateState} can be used to manually invoke a update.
 */
export class DOMOutput extends OutputBase {
  shadow: ShadowRoot;
  contentNode: HTMLDivElement;

  shadowContainer;
  errorContainer;

  cssSheets: Map<CodeInput, CSSStyleSheet> = new Map();
  htmlSources: Map<CodeInput, HTMLSourceMap> = new Map();
  jsFunctions: Map<CodeInput, () => void> = new Map();

  /** @internal */
  _staticCSSSources;


  constructor(staticStyleSheets?: string[], name = 'Output') {
    super(name, 'isd-dom-output');

    this.contentNode = document.createElement('div');

    this.shadowContainer = document.createElement('div');
    this.shadowContainer.classList.add('isd-dom-shadow-container');

    this.errorContainer = document.createElement('div');
    this.errorContainer.classList.add('isd-dom-error-container');

    this._staticCSSSources = staticStyleSheets;
  }

  // ---- Events ----
  override update(input: InputBase) {
    if (input instanceof CodeInput) {
      // For SyntaxErrors that throw on code evaluation.
      // Because of this, there's no column/line information available to extract.
      try {
        this.bindStateFromSingleInput(input);
      } catch (e) {
        if (e instanceof SyntaxError) {
          this.setErrorContent(e.toString());
          return;
        } else throw e;
      }

      this.hideErrorContent();
      this.updateState();
    }
  }
  override build(panel: HTMLElement) {
    this.shadow = this.shadowContainer.attachShadow({ mode: 'closed' });

    if (this._staticCSSSources) {
      for (const css of this._staticCSSSources) {
        const styleSheet = DOMOutput.getStyleSheet(css);
        this.shadow.adoptedStyleSheets.push(styleSheet);
      }
      delete this._staticCSSSources;
    }

    this.shadow.appendChild(this.contentNode);

    panel.appendChild(this.shadowContainer);
    panel.appendChild(this.errorContainer);
  }
  override setup(inputs: InputBase[]) {
    const order = [ 'css', 'html', 'js' ];

    const sortedInputs = <CodeInput[]> inputs
      .filter(input => input instanceof CodeInput && order.includes(input.language))
      .sort((a, b) => {
        return order.indexOf((a as CodeInput).language) >= order.indexOf((b as CodeInput).language)
          ? -1
          : 1
      });

    for (const input of sortedInputs) {
      this.bindStateFromSingleInput(input);
    }

    this.updateState();
  }

  // ---- State functions ----
  /**
   * Bind the given {@link CodeInput} (either JS, HTML or CSS) to
   * the shadow DOM, processing its source.
   *
   * Except for the CSS, this does not yet update the Shadow DOM, only prepare
   * it for an update. You will want to call {@link updateState} after binding.
   */
  bindStateFromSingleInput(input: CodeInput) {
    const source = input.getSource();

    switch (input.language) {
      case 'css': {
        if (this.cssSheets.has(input)) {
          this.cssSheets.get(input).replaceSync(source);
        } else {
          const styleSheet = DOMOutput.getStyleSheet(source);
          this.shadow.adoptedStyleSheets.push(styleSheet);
          this.cssSheets.set(input, styleSheet);
        }
        break;
      }
      case 'html': {
        if (this.htmlSources.has(input)) {
          this.htmlSources.get(input).source = source;
        } else {
          const wrapper = document.createElement('div');
          this.contentNode.appendChild(wrapper);
          this.htmlSources.set(input, { source, wrapper });
        }
        break;
      }
      case 'js': {
        const callback = this.convertJSStringToFunction(source);
        this.jsFunctions.set(input, callback);
        break;
      }
    }
  }

  /** Reset the HTML content and call every bound JavaScript anew. */
  updateState() {
    this.#resetHTML();
    this.#callJS();
  }

  // ---- DOM state handling ----
  #resetHTML() {
    for (const { wrapper, source } of this.htmlSources.values()) {
      wrapper.textContent = '';
      wrapper.insertAdjacentHTML('beforeend', source);
    }
  }
  #callJS() {
    for (const jsFunc of this.jsFunctions.values()) {
      // Catches all errors inside the user's code
      try {
        jsFunc();
      } catch (e) {
        const position = DOMOutput.getAnonymousFnErrorPosition(e as Error);
        let errorInfo = e.toString();
        if (position) {
          errorInfo += `\n-> Line ${position[0]}, Column ${position[1]}`
        }
        this.setErrorContent(errorInfo);
      }
    }
  }

  // ---- DOM Helper functions ----
  /**
   * Set the content of the error modal and display it.
   * @param errorStr The error message.
   * @see {@link hideErrorContent}
   */
  setErrorContent(errorStr: string) {
    errorStr = errorStr.replace(/\n/g, '<br>');
    this.errorContainer.innerHTML = errorStr;
    this.errorContainer.classList.add('isd-dom-active');
  }
  /**
   * Hide the error modal again.
   * @see {@link setErrorContent}
   */
  hideErrorContent() {
    this.errorContainer.classList.remove('isd-dom-active');
  }

  // ---- Helper functions ----
  /**
   * Convert a JavaScript string to an executable function that is bound
   * to the current shadow DOM element as its `this`.
   *
   * Does not use `eval`.
   *
   * @see {@link shadowfyJSString}
   */
  convertJSStringToFunction(jsString: string) {
    const preparedJSString = DOMOutput.shadowfyJSString(jsString);
    return (new Function(preparedJSString)).bind(this.shadow);
  }


  // ---- Static helper functions
  /**
   * Prepare a JavaScript string for use in a shadow DOM.
   *
   * Namely, this replaces all occurences of `document` with `this`
   * because a shadow DOM is not self contained and still exists
   * in the scope of the parent document
   * (Also see https://github.com/tc39/proposal-shadowrealm).
   *
   * @see {@link convertJSStringToFunction}
   */
  static shadowfyJSString(jsString: string) {
    return jsString.trim().replace(/document/g, 'this');
  }

  /**
   * Create a {@link CSSStyleSheet} with the given CSS source
   * for adoption by a shadom DOM.
   */
  static getStyleSheet(cssString: string) {
    const styleSheet = new CSSStyleSheet();
    styleSheet.replaceSync(cssString);
    return styleSheet;
  }

  /**
   * Get the line and column position from an error. Highly unstable.
   *
   * @remarks
   * This reads the stack trace string given with an error.
   * It's really esotheric and heavily relies on vendor-specific implementation,
   * which is not a recipe for disaster at all. But hey, it works (for now)!
   *
   * Only works for Firefox and Chrome.
   *
   * @return [ line, column ].
   */
  static getAnonymousFnErrorPosition(error: Error): [ number, number ] | false {
    // @ts-ignore
    if (!error.line) {
      const stackEntryRegex = new RegExp(`anonymous.*${import.meta.url}|${import.meta.url}.*anonymous`);
      const relevantStackEntry = error.stack
        .split('\n')
        .filter(val => stackEntryRegex.test(val))[0];

      const matchResult = relevantStackEntry.match(/(\d+):(\d+).?$/);
      if (matchResult) {
        return [
          parseInt(matchResult[1]) - 2,
          parseInt(matchResult[2])
        ];
      }
    }
    return false;
  }
}
