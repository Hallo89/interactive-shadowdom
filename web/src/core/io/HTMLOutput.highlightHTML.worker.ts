onmessage = (e) => {
  importScripts('../../highlightjs/highlight.html.min.js');
  // @ts-ignore TS support for Workers is lacking
  const result = self.hljs.highlight(e.data, { language: 'html' });
  postMessage(result.value);
};
